package com.micro.fast.boot.starter.common.exception;


/**
 * 系统级别的异常,比如数据哭插入失败
 * @author lsy
 */
public class SystemException extends RuntimeException {
  public SystemException(String message) {
    super(message);
  }
}
