package com.micro.fast.upms.pojo;

import java.io.Serializable;

public class UpmsUserOrganization implements Serializable {
    private Integer userOrganizationId;

    private Integer userId;

    private Integer organizationId;

    private static final long serialVersionUID = 1L;

    public UpmsUserOrganization(Integer userOrganizationId, Integer userId, Integer organizationId) {
        this.userOrganizationId = userOrganizationId;
        this.userId = userId;
        this.organizationId = organizationId;
    }

    public UpmsUserOrganization() {
        super();
    }

    public Integer getUserOrganizationId() {
        return userOrganizationId;
    }

    public void setUserOrganizationId(Integer userOrganizationId) {
        this.userOrganizationId = userOrganizationId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userOrganizationId=").append(userOrganizationId);
        sb.append(", userId=").append(userId);
        sb.append(", organizationId=").append(organizationId);
        sb.append("]");
        return sb.toString();
    }
}