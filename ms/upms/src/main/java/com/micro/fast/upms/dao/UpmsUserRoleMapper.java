package com.micro.fast.upms.dao;

import com.micro.fast.upms.pojo.UpmsUserRole;

public interface UpmsUserRoleMapper {
    int deleteByPrimaryKey(Integer userRoleId);

    int insert(UpmsUserRole record);

    int insertSelective(UpmsUserRole record);

    UpmsUserRole selectByPrimaryKey(Integer userRoleId);

    int updateByPrimaryKeySelective(UpmsUserRole record);

    int updateByPrimaryKey(UpmsUserRole record);
}