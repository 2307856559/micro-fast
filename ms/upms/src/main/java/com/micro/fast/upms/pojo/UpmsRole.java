package com.micro.fast.upms.pojo;

import java.io.Serializable;

public class UpmsRole implements Serializable {
    private Integer id;

    private String name;

    private String description;

    private Long ctime;

    private Long orders;

    private static final long serialVersionUID = 1L;

    public UpmsRole(Integer id, String name, String description, Long ctime, Long orders) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.ctime = ctime;
        this.orders = orders;
    }

    public UpmsRole() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", description=").append(description);
        sb.append(", ctime=").append(ctime);
        sb.append(", orders=").append(orders);
        sb.append("]");
        return sb.toString();
    }
}